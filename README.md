# shtatic

A static site generator based in the shell.

## Installing

Requirements:

- a POSIX-compatible shell
- esh: https://github.com/jirutka/esh

## License

[Apache 2.0](./LICENSE)
